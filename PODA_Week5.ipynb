{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3aa6f666",
   "metadata": {},
   "source": [
    "# Welcome to Week 5 of your PODA computer labs\n",
    "\n",
    "This week, we will look into variable relationships. Specifically, we will start with correlation analysis and then focus on linear regression.\n",
    "\n",
    "The dataset we're going to use this week comes from the scikit learn package. This allows us to make use of the dataset documentation as well as the pre-defined independent and dependent variables within it.\n",
    "\n",
    "Specifically, we will work with the Diabetes dataset. You can have a look at the documentation for that data [here](https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "id": "bb0e6511",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".. _diabetes_dataset:\n",
      "\n",
      "Diabetes dataset\n",
      "----------------\n",
      "\n",
      "Ten baseline variables, age, sex, body mass index, average blood\n",
      "pressure, and six blood serum measurements were obtained for each of n =\n",
      "442 diabetes patients, as well as the response of interest, a\n",
      "quantitative measure of disease progression one year after baseline.\n",
      "\n",
      "**Data Set Characteristics:**\n",
      "\n",
      "  :Number of Instances: 442\n",
      "\n",
      "  :Number of Attributes: First 10 columns are numeric predictive values\n",
      "\n",
      "  :Target: Column 11 is a quantitative measure of disease progression one year after baseline\n",
      "\n",
      "  :Attribute Information:\n",
      "      - age     age in years\n",
      "      - sex\n",
      "      - bmi     body mass index\n",
      "      - bp      average blood pressure\n",
      "      - s1      tc, total serum cholesterol\n",
      "      - s2      ldl, low-density lipoproteins\n",
      "      - s3      hdl, high-density lipoproteins\n",
      "      - s4      tch, total cholesterol / HDL\n",
      "      - s5      ltg, possibly log of serum triglycerides level\n",
      "      - s6      glu, blood sugar level\n",
      "\n",
      "Note: Each of these 10 feature variables have been mean centered and scaled by the standard deviation times the square root of `n_samples` (i.e. the sum of squares of each column totals 1).\n",
      "\n",
      "Source URL:\n",
      "https://www4.stat.ncsu.edu/~boos/var.select/diabetes.html\n",
      "\n",
      "For more information see:\n",
      "Bradley Efron, Trevor Hastie, Iain Johnstone and Robert Tibshirani (2004) \"Least Angle Regression,\" Annals of Statistics (with discussion), 407-499.\n",
      "(https://web.stanford.edu/~hastie/Papers/LARS/LeastAngle_2002.pdf)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "import sklearn.datasets as ds\n",
    "\n",
    "df = ds.load_diabetes()\n",
    "\n",
    "print(df.DESCR) #This gives us a quick summary of the dataset. The DESCR function is a special function for these scikit learn datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "id": "9617b276",
   "metadata": {},
   "outputs": [],
   "source": [
    "# For some of our later analysis it's more convenient to transform the data into a pandas dataframe\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "df = pd.DataFrame(df['data'], #specifies that we just want to look at the data columns (our independent variables)\n",
    "            columns = df.feature_names) # keeps column names intact "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d33a7d6",
   "metadata": {},
   "source": [
    "You will notice that each of our values has already been mean centered and scaled for us, so we can proceed without the need of additional pre-processing to our correlation analysis and regression."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54861097",
   "metadata": {},
   "source": [
    "# Section 1: Correlation analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1f188f1",
   "metadata": {},
   "source": [
    "### 1.1 Visualisation and plotting\n",
    "\n",
    "**TASK**\n",
    "\n",
    "In a first step, we now to visualise how our variables relate to each other. A very nice way to do that is using the seaborn function pairplot().\n",
    "\n",
    "To limit our output, I would recommend focusing on just a couple of variables. But feel free to plot different combinations and check out how this function can be used.\n",
    "\n",
    "In this example, I want to plot the six different blood test results (s1, s2, ..., s6). So, I first create a sub dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "id": "ac4ac87b",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_sub = df[[\"s1\", \"s2\", \"s3\", \"s4\", \"s5\", \"s6\"]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2cefdb8",
   "metadata": {},
   "source": [
    "Using the df_sub above and the pairplot() function in seaborn, plot the correlations between these six variables. Look at the plots and check whether there are any correlations that you can see immediately from the plots.\n",
    "\n",
    "Warning: This function can be computationally a little expensive, so please be patient while your plot is being created.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "140a7322",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9465dcf5",
   "metadata": {},
   "source": [
    "### 1.2 Correlation coefficients\n",
    "\n",
    "After the first visualisation, we are interested in the actual correlation coefficients.\n",
    "\n",
    "The easiest way to create a correlation matrix is using the corr() function from pandas. Start by reading the documentation for that function [here](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.corr.html).\n",
    "\n",
    "You will notice that one of the parameters for that function is the different correlation metrics that can be used. You can choose between\n",
    "\n",
    "- pearson : Pearson correlation coeffcient (for continuous data which are normally distributed, homoscedastic and have a linear relationship)\n",
    "- kendall : Kendall Tau correlation coefficient (for at least ordinal data; doesn't require normal distribution)\n",
    "- spearman : Spearman rank correlation (for at least ordinal data; doesn't require normal distribution)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2235fc4f",
   "metadata": {},
   "source": [
    "**TASK**\n",
    "\n",
    "Using the pandas corr() function, print a correlation matrix of the variables in your dataframe. For illustration purposes, you can choose any correlation coefficient for this or try different ones and compare the results. You can also try this for the whole dataframe (df) or continue working with the subset from earlier (df_sub)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6fdb47c3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b319753f",
   "metadata": {},
   "source": [
    "**TASK**\n",
    "\n",
    "Now that we have created our correlation matrix, we can alsoa have a look at another way of visualising the result. \n",
    "\n",
    "Again, seaborn provides us with beautiful and very convenient plotting functions.\n",
    "\n",
    "Have a look at the seaborn function [heatmap()](https://seaborn.pydata.org/generated/seaborn.heatmap.html). Try creating one and use the correlation matrix you created in the previous task as the input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "41ec152d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db1197e3",
   "metadata": {},
   "source": [
    "## Section 2: Linear regression\n",
    "\n",
    "We now want to implement a linear regression model using our diabetes dataset.\n",
    "\n",
    "First, we have to define what is the independent and what is the dependent part of our data. With the Diabetes data from sklearn, these are already defined for us. As a reminder, the documentation for the data can be found [here](https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset). You can see that there are two parts to the data: 'data' and 'target'.\n",
    "\n",
    "- 'target' will be our y - it's described as \"a quantitative measure of disease progression one year after baseline\"\n",
    "- 'data' will be our X. We've already looked at the different features above and will now use them to try and predict y.\n",
    "\n",
    "Our first step is now to separate those into two separate pandas dataframes. That makes it easier to use the inbuilt linear regression functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "id": "cd3215cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "X = pd.DataFrame(data = dataset_class['data'], columns = dataset_class['feature_names'])\n",
    "y = pd.DataFrame(data = dataset_class['target'], columns = ['target'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da44b75b",
   "metadata": {},
   "source": [
    "### 2.1 Simple linear regression\n",
    "\n",
    "**TASK**\n",
    "\n",
    "We will start with a simple linear regression model with just one explanatory variable X.\n",
    "\n",
    "You can choose any of the variables which interest you. I will pick 'bp' which is the average blood pressure.\n",
    "\n",
    "I would recommend using the statsmodels function for model building. You should first have a look at the documentation [here](https://www.statsmodels.org/stable/regression.html).\n",
    "\n",
    "Using that function and the OLS estimator, create a simple linear regression model and print the summary of it using the summary() function.\n",
    "\n",
    "You can closely follow the example in the documentation above.\n",
    "\n",
    "Tip: You will notice and see in the documentation that you will have to add a constant to your X for this model to work. I do that using sm.add_constant(X)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "ed423540",
   "metadata": {},
   "outputs": [],
   "source": [
    "import statsmodels.api as sm\n",
    "\n",
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55e19a2d",
   "metadata": {},
   "source": [
    "### 2.2 Multiple linear regression\n",
    "\n",
    "**TASK**\n",
    "\n",
    "Now let's have a look what changes when we add all our variables to the model. Using the same statsmodel OLS() function from above, this time include all of the variables in X. Print the model summary and compare your results to those above. What do you notice? Does your model's explanatory power improve?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "94e91098",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0beac882",
   "metadata": {},
   "source": [
    "### Optional extra tasks:\n",
    "\n",
    "Have a closer look at the regression output, in particular the significance of your coefficients. What do you notice? Think back to your correlation analysis from earlier. You can consider removing some of those stronger correlated variables and re-run the linear regression with a smaller number of X variables. What changes regarding your model fit?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
